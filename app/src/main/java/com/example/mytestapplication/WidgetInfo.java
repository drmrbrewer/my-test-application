package com.example.mytestapplication;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

class WidgetInfo {
    private static final String TAG = "WidgetInfo";

    final int pxWidth;
    final int pxHeight;
    final int dpWidth;
    final int dpHeight;
    private final float density;
    private final int orientation;
    final String strOrientation;

    private WidgetInfo(int dpWidth, int dpHeight, float density, int orientation) {
        this.dpWidth = dpWidth;
        this.dpHeight = dpHeight;
        this.density = density;
        this.orientation = orientation;
        this.pxWidth = getWidgetDimensionsInPx(dpWidth, density);
        this.pxHeight = getWidgetDimensionsInPx(dpHeight, density);
        this.strOrientation = getStrOrientation(orientation);
        Log.d(TAG, "widget dimensions in dp: " + this.dpWidth + " x " + this.dpHeight);
        Log.d(TAG, "widget dimensions in px: " + this.pxWidth + " x " + this.pxHeight);
        Log.d(TAG, "screen display density: " + density);
    }

    static WidgetInfo getWidgetInfo(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        Resources resources = context.getResources();
        int orientation = resources.getConfiguration().orientation;
        float density = resources.getDisplayMetrics().density;
        int[] dimensionsDp = getWidgetDimensionsInDp(context, appWidgetManager, appWidgetId, orientation);
        return new WidgetInfo(Math.round(dimensionsDp[0]), Math.round(dimensionsDp[1]), density, orientation);
    }

    private static int getWidgetDimensionsInPx(int dimensionsDp, float density) {
        return Math.round(dimensionsDp * density);
    }

    private static int[] getWidgetDimensionsInDp(Context context, AppWidgetManager appWidgetManager, int appWidgetId, int orientation) {

        // determine widget dimensions from appWidgetManager.getAppWidgetOptions()
        // see: https://developer.android.com/guide/topics/appwidgets#AppWidgetProvider
        // and: https://stackoverflow.com/a/18723268/4070848

        Bundle appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId);

        int minWidth = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH);
        int minHeight = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);
        int maxWidth = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH);
        int maxHeight = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT);

        Log.d(TAG, "minWidth <-> maxWidth x minHeight <-> maxHeight: " + minWidth + " <-> " + maxWidth + " x " + minHeight + " <-> " + maxHeight);

        int width, height;

        String strOrientation = getStrOrientation(orientation);

        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                Log.d(TAG, "landscape orientation");
                width = maxWidth;
                height = minHeight;
                break;
            case Configuration.ORIENTATION_PORTRAIT:
            case Configuration.ORIENTATION_UNDEFINED:
            default:
                // assume portrait if not landscape
                Log.d(TAG, "portrait orientation");
                width = minWidth;
                height = maxHeight;
        }

        Toast.makeText(context, strOrientation + ": " + width + "dp x " + height + "dp", Toast.LENGTH_SHORT).show();

        return new int[] { width, height };
    }

    private static String getStrOrientation (int orientation) {
        String strOrientation;
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                strOrientation = "portrait";
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                strOrientation = "landscape";
                break;
            case Configuration.ORIENTATION_UNDEFINED:
            default:
                strOrientation = "unknown";
        }
        return strOrientation;
    }
}
