package com.example.mytestapplication;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link MyAppWidgetConfigureActivity MyAppWidgetConfigureActivity}
 */
public class MyAppWidget extends AppWidgetProvider {

    private static final String TAG = "MyAppWidget";

    // the following should match what is registered as an intent-filter in AndroidManifest.xml
    static final String WIDGET_CLICK = "com.example.mytestapplication.WIDGET_CLICK";

    static void updateAppWidget(Context context, int appWidgetId) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        updateAppWidget(context, appWidgetManager, appWidgetId);
    }

    private static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        CharSequence widgetText = MyAppWidgetConfigureActivity.loadTitlePref(context, appWidgetId);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.my_app_widget);

        PendingIntent pendingIntent = createWidgetClickIntent(context, appWidgetId);
        remoteViews.setOnClickPendingIntent(R.id.appwidget_text, pendingIntent);

        WidgetInfo widgetInfo = WidgetInfo.getWidgetInfo(context, appWidgetManager, appWidgetId);

        Bitmap bitmap = getBitmap(widgetInfo);
        if (bitmap != null) {
            remoteViews.setImageViewBitmap(R.id.appwidget_image, bitmap);
        }

        String strInfo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()) + " | " +
                widgetInfo.strOrientation + " | " +
                widgetInfo.dpWidth + "dp x " + widgetInfo.dpHeight + "dp | " +
                widgetInfo.pxWidth + "px x " + widgetInfo.pxHeight + "px";
        remoteViews.setTextViewText(R.id.appwidget_info, strInfo);

        remoteViews.setTextViewText(R.id.appwidget_text, widgetText);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            MyAppWidgetConfigureActivity.deleteTitlePref(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        initialiseAllWidgets(context);
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(final Context context, Intent intent) {

        // must keep the following, so that the parent class can dispatch broadcasts to the various helper functions in this class
        super.onReceive(context, intent);

        Bundle extras = intent.getExtras();

        int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        if (extras != null) {
            appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        String action = intent.getAction();
        Log.d(TAG, "widget click action: " + action);

        if (WIDGET_CLICK.equals(action)) {
            updateAppWidget(context, appWidgetId);
        }
    }

    private static void initialiseAllWidgets(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, MyAppWidget.class));
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    private static PendingIntent createWidgetClickIntent(Context context, int appWidgetId) {
        context = context.getApplicationContext();
        Intent intent = new Intent(WIDGET_CLICK);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setPackage(context.getPackageName());
        return PendingIntent.getBroadcast(context, appWidgetId, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }

    private static Bitmap getBitmap(WidgetInfo widgetInfo) {
        Bitmap bitmap = null;
        if (widgetInfo.pxWidth > 0 && widgetInfo.pxHeight > 0) {
            bitmap = Bitmap.createBitmap(widgetInfo.pxWidth, widgetInfo.pxHeight, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(android.graphics.Color.GREEN);
        }
        return bitmap;
    }
}

